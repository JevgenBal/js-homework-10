let ul = document.querySelector('.tabs');
ul.classList.add('active');


ul.addEventListener('click', function (ev) {
    let tabsContent = document.querySelectorAll('.tabs-content li');
    tabsContent.forEach(function (tab) {
        tab.classList.remove('active-p');
    });
    let data = ev.target.dataset.type;
    let tabContent = document.querySelector(`[data-li="${data}"]`);
    tabContent.classList.add('active-p');
    let activeTab = document.querySelector('.active-tab');
    if (activeTab) {
        activeTab.classList.remove('active-tab');
    }
    ev.target.classList.add('active-tab');
});
